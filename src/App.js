import React, { Component } from "react";
import Chat from "./components/chat";
class App extends Component {
  render() {
    return (
      <section>
        <Chat />
      </section>
    );
  }
}

export default App;
