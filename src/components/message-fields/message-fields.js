import React from "react";
import { Wrapper, InputField, SendButton } from "./message-fields.style";
import PropTypes from "prop-types";

class MessageFields extends React.Component {
  static propTypes = {
    onSendMessage: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      message: " ",
      disableSubmit: true
    };
  }

  handleChange(e) {
    if (e.target.value) {
      return this.setState({ message: e.target.value, disableSubmit: false });
    }
    this.setState({ disableSubmit: true });
  }

  handleKeyPress(e) {
    if (e.key === "Enter") {
      this.submit();
    }
  }

  submit() {
    if (this.state.message !== " ") {
      this.props.onSendMessage(this.state.message);
      this.setState({ message: "", enableSubmit: false });
    }
  }

  render() {
    return (
      <Wrapper>
        <InputField
          type="text"
          data-testid="message-field"
          onChange={e => this.handleChange(e)}
          onKeyPress={e => this.handleKeyPress(e)}
          value={this.state.message}
        />
        <SendButton
          data-testid="send-button"
          type="submit"
          onClick={() => this.submit()}
          disabled={this.state.disableSubmit}
        >
          Enviar
        </SendButton>
      </Wrapper>
    );
  }
}

export default MessageFields;
