import styled, { css } from "react-emotion";

export const Wrapper = styled("div")`
  display: flex;
`;

export const InputField = styled("input")``;

export const SendButton = styled("button")`
  ${props =>
    props.disabled &&
    css`
      cursor: not-allowed;
    `};
`;
