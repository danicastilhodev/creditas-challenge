import React from "react";
import { render, fireEvent } from "react-testing-library";
import MessageFields from "./message-fields";

describe("<MessageFields/>", () => {
  it("should call submit function", () => {
    const handleClick = jest.fn();
    const { getByTestId } = render(
      <MessageFields onSendMessage={handleClick} />
    );
    const sendButton = getByTestId("send-button");
    const inputMessage = getByTestId("message-field");
    const textMessage = "text message";

    fireEvent.change(inputMessage, { target: { value: textMessage } });
    fireEvent.click(sendButton);
    expect(handleClick).toHaveBeenCalledTimes(1);
  });

  it("should not call submit if field is empty", () => {
    const handleClick = jest.fn();
    const { getByTestId } = render(
      <MessageFields onSendMessage={handleClick} />
    );
    const sendButton = getByTestId("send-button");
    const inputMessage = getByTestId("message-field");
    const textMessage = " ";

    fireEvent.change(inputMessage, { target: { value: textMessage } });
    fireEvent.click(sendButton);
    expect(handleClick).toHaveBeenCalledTimes(0);
  });

  it("should call submit on keyPress", () => {
    const handleClick = jest.fn();
    const { getByTestId } = render(
      <MessageFields onSendMessage={handleClick} />
    );
    const inputMessage = getByTestId("message-field");
    const textMessage = "text message";

    fireEvent.change(inputMessage, { target: { value: textMessage } });
    fireEvent.keyPress(inputMessage, { keyCode: 13 });
    expect(handleClick).toHaveBeenCalledTimes(1);
  });

  it("should not call submit on keyPress if field is empty", () => {
    const handleClick = jest.fn();
    const { getByTestId } = render(
      <MessageFields onSendMessage={handleClick} />
    );
    const inputMessage = getByTestId("message-field");
    const textMessage = " ";

    fireEvent.change(inputMessage, { target: { value: textMessage } });
    fireEvent.keyPress(inputMessage, { keyCode: 13 });
    expect(handleClick).toHaveBeenCalledTimes(0);
  });
});
