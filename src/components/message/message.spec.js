import React from "react";
import { render } from "react-testing-library";
import Message from "./message";

describe("<Message />", () => {
  it("should render message", () => {
    const message = "text message";
    const { getByTestId } = render(<Message message={message} />);

    expect(getByTestId("message")).toHaveTextContent(message);
  });
});
