import React from "react";
import { string } from "prop-types";
import SingleMessage from "./message.style";

const Message = ({ message }) => (
  <SingleMessage data-testid="message">{message}</SingleMessage>
);

Message.defaultProps = {
  message: ""
};

Message.propTypes = {
  message: string
};

export default Message;
