import styled from "react-emotion";

const SingleMessage = styled("li")`
  width: 20em;
`;

export default SingleMessage;
