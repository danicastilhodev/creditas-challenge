import React from "react";
import { render, fireEvent } from "react-testing-library";
import Chat from "./chat";

describe("<Chat />", () => {
  it("should send a message on click submit button", () => {
    const textMessage = "text message";
    const { getByTestId, getByText } = render(<Chat />);
    const fieldMessage = getByTestId("message-field");
    const sendButton = getByTestId("send-button");
    fireEvent.change(fieldMessage, { target: { value: textMessage } });
    fireEvent.click(sendButton);

    expect(getByText(textMessage)).toBeInTheDocument();
  });
});
