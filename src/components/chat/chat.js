import React from "react";
import MessageList from "../message-list/message-ilst";
import MessageFields from "../message-fields";
import { WrapperChat, Title } from "./chat.style";

class Chat extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      listMessage: []
    };
    this.sendMessage = this.sendMessage.bind(this);
  }
  sendMessage(message) {
    this.setState({
      listMessage: [...this.state.listMessage, { text: message }]
    });
  }
  render() {
    return (
      <WrapperChat>
        <Title>Chat</Title>
        <MessageList messages={this.state.listMessage} />
        <MessageFields onSendMessage={this.sendMessage} />
      </WrapperChat>
    );
  }
}

export default Chat;
