import React from "react";
import Message from "../message";
import List from "./message-list.style";
import { string, number, arrayOf, shape } from "prop-types";

const MessageList = ({ messages }) => (
  <List data-testid="list-message">
    {messages.map((message, id) => (
      <Message message={message.text} key={id} />
    ))}
  </List>
);

MessageList.defaultProps = {
  messages: []
};

MessageList.proptypes = {
  messages: arrayOf(
    shape({
      id: number,
      message: string
    })
  )
};

export default MessageList;
