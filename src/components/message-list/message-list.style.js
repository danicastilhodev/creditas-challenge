import styled from "react-emotion";

const List = styled("ul")`
  min-height: 10em;
  background-color: #f3f3f3;
`;

export default List;
