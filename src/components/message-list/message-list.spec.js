import React from "react";
import { render } from "react-testing-library";
import MessageList from "./message-ilst";

describe("<MessageList />", () => {
  it("should render a list of messages", () => {
    const listMessages = [
      { text: "first message" },
      { text: "second message" }
    ];

    const { getByText } = render(<MessageList messages={listMessages} />);
    expect(getByText(listMessages[0].text)).toBeInTheDocument();
    expect(getByText(listMessages[1].text)).toBeInTheDocument();
  });
});
