# Creditas Challenge

Chat desenvolvido com base no `create-react-app`, para o desafio de Front-end.

## Tecnologias utilizadas

- React
- Emotion
- React Testing Library
- Jest DOM
- Cypress

## Instalando / Iniciando

Para inicializar o projeto:

```shell
git clone https://gitlab.com/danicastilhodev/creditas-challenge.git
cd creditas-challenge
npm install
npm start
```

### Testes unitários

`npm test`

### Testes de integração

`npm run cypress:dev`

Em seguida, clicar sobre `chat.spec.js`

### Build

`npm run build`
