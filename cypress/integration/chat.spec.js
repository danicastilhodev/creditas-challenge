describe("Chat app test", () => {
  it("should send a message", () => {
    cy.visit("http://localhost:3000");
    cy.get("[data-testid=message-field]").type("simple messsage");
    cy.get("[data-testid=send-button").click();

    cy.get("[data-testid=list-message").should("have.length", 1);
  });

  it("should cannot send a empty message", () => {
    cy.visit("http://localhost:3000");

    cy.get("[data-testid=send-button").should("be.disabled");
  });
});
